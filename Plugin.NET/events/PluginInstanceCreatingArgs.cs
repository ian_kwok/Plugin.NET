﻿using System;
using System.Reflection;

namespace PluginNET.events
{
    /// <summary>
    /// 创建插件中的类实例前的事件数据
    /// </summary>
    public class PluginInstanceCreatingArgs : PluginEventArgs
    {
        /// <summary>
        /// 程序集对象
        /// </summary>
        public Assembly Assembly { get; set; }

        /// <summary>
        /// 加载插件类成功后读取到的类
        /// </summary>
        public Type Class { get; internal set; }
    }
}
